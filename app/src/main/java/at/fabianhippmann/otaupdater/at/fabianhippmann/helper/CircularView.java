package at.fabianhippmann.otaupdater.at.fabianhippmann.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import at.fabianhippmann.otaupdater.R;

/**
 * Created by ANON on 13.12.2014.
 */
public class CircularView extends View {

    private final int mColor;

    public CircularView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircularView, 0,0);
        try {
            mColor = a.getColor(R.styleable.CircularView_color, 0);
        } finally{
            a.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int w = this.getWidth();
        int h = this.getHeight();
        int radius = w/2;
        int wc = w/2;
        int hc = h/2;
        canvas.drawCircle(wc,hc, radius, getFill() );
    }

    private Paint getFill(){
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
        p.setColor(mColor);
        p.setStyle(Paint.Style.FILL);
        return p;
    }
}
