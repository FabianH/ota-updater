package at.fabianhippmann.otaupdater;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;

import at.fabianhippmann.otaupdater.utils.SecretConstants;

/**
 * Created by ANON on 13.12.2014.
 */
public class UpdaterApplication extends Application{
    @Override
    public void onCreate() {

        Parse.enableLocalDatastore(this);
        Parse.initialize(this, SecretConstants.APPLICATION_ID, SecretConstants.CLIENT_KEY);
        PushService.setDefaultPushCallback(this, MainActivity.class);

        ParseInstallation.getCurrentInstallation().saveInBackground();


    }
}
