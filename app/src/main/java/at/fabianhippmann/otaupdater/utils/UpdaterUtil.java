package at.fabianhippmann.otaupdater.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ANON on 13.12.2014.
 */
public class UpdaterUtil {

    public static String getCurrentCMVersion(){
        Process p = null;

        try {
            p = new ProcessBuilder("/system/bin/getprop", "ro.cm.display.version").redirectErrorStream(true).start();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            while((line = bufferedReader.readLine()) != null){
                return line;
            }
            p.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "error";

    }
}
