package at.fabianhippmann.otaupdater.utils;


public class ParseConstants {
    public static final String VERSION_NAME = "Name";
    public static final String VERSION_CHANGELOG = "changelog";
    public static final String VERSION_RELEASE_DATE = "releaseDate";
    public static final String VERSION_NUMBER = "versionNumber";
    public static final String VERSION_DOWNLOAD = "downloadLink";
    public static final String VERSION_WIPESYSTEM = "wipeSystem";
    public static final String VERSION_WIPECACHE = "wipeCache";
    public static final String VERSION_WIPEDATA = "wipeData";

}
