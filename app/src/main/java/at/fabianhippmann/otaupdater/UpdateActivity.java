package at.fabianhippmann.otaupdater;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.File;
import java.text.DecimalFormat;

import at.fabianhippmann.otaupdater.utils.ParseConstants;


public class UpdateActivity extends Activity {

    private ScrollView mButtonBackgroundView;
    private ParseObject mVersionObject;
    private TextView mVersionTextView;
    private TextView mTextcontentview;
    private LinearLayout mPercentageContainer;
    private TextView mPercentageTextView;
    private Button mDownloadButton;
    private Button mInstallRomButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        mButtonBackgroundView = (ScrollView) findViewById(R.id.transitiontoview);
        mButtonBackgroundView.setTransitionName("buttonBackground");

        mVersionTextView = (TextView) findViewById(R.id.version);
        mTextcontentview = (TextView) findViewById(R.id.textContent);

        mPercentageContainer = (LinearLayout) findViewById(R.id.percentagecontainer);
        mPercentageTextView = (TextView) findViewById(R.id.percentage);
        String objectId = getIntent().getStringExtra("objectId");
        mDownloadButton = (Button) findViewById(R.id.downloadrom);
        mInstallRomButton = (Button) findViewById(R.id.installrom);
        ParseQuery query = ParseQuery.getQuery("VersionObject");
        query.getInBackground(objectId, new GetCallback() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                mVersionObject = parseObject;
                mVersionTextView.setText("Version: "+parseObject.getString(ParseConstants.VERSION_NUMBER));
                mTextcontentview.setText(Html.fromHtml(parseObject.getString(ParseConstants.VERSION_CHANGELOG)));
            }
        });

    }

    public void installRom(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(UpdateActivity.this);
        String wipeSystemMessage =" wipe System,";
        String wipeCacheMessage = "wipe Cache";
        String wipeDataMesssage =", wipe Data (will delete all your apps";

        StringBuilder message = new StringBuilder();
        String prefix = "To update you should: ";
        message.append(prefix);
        if(mVersionObject.getBoolean(ParseConstants.VERSION_WIPESYSTEM)) message.append(wipeSystemMessage);
        if(mVersionObject.getBoolean(ParseConstants.VERSION_WIPECACHE)) message.append(wipeCacheMessage);
        if(mVersionObject.getBoolean(ParseConstants.VERSION_WIPEDATA)) message.append(wipeDataMesssage);

        builder.setMessage(message.toString());
        builder.setTitle("Install rom");
        builder.setPositiveButton("Boot to recovery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rebootToRecovery();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void rebootToRecovery(){
        try {
            Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", "reboot recovery" });
            proc.waitFor();
        } catch (Exception ex) {
            Log.i("fail", "Could not reboot", ex);
        }


    }
    public void download(View view) {
        if (mVersionObject != null) {
            Ion.getDefault(UpdateActivity.this).configure().setLogging("MyLogs", Log.DEBUG);
            mDownloadButton.setVisibility(View.GONE);
            File dlFile = new File("sdcard/" + mVersionObject.getString(ParseConstants.VERSION_NAME)+ ".zip");
            if (dlFile.exists()) {
                dlFile.delete();
                Log.d("filepath", dlFile.getAbsolutePath());
            }
            Ion.with(UpdateActivity.this)
           .load(mVersionObject.getString(ParseConstants.VERSION_DOWNLOAD))
           .progressHandler(new ProgressCallback() {
               @Override
               public void onProgress(long downloaded, long total) {

                   float percentage =  (float) downloaded/total * 100;
                   DecimalFormat df = new DecimalFormat("#.#");
                   mPercentageTextView.setText(df.format(percentage) + " %");
               }
           })
           .write(dlFile).setCallback(new FutureCallback<File>() {
                @Override
                public void onCompleted(Exception e, File result) {
                    if (e == null) {
                        Log.i("sucessful", "gogo");
                    } else {
                        Log.e("error", e.getMessage());
                    }
                }
            });



            mPercentageContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}



