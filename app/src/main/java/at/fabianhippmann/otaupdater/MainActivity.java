package at.fabianhippmann.otaupdater;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.List;

import at.fabianhippmann.otaupdater.utils.ParseConstants;
import at.fabianhippmann.otaupdater.utils.UpdaterUtil;


public class MainActivity extends Activity {


    private View mButtonBackgroundView;
    private RelativeLayout mUpdateContainer;
    private RelativeLayout mNoUpdateContainer;
    private ParseObject mCurrentVersionObject;
    private ParseObject mNewVersionObject;
    private TextView mCurrentVersionTextView;
    private TextView mUpdateSubTitleTextView;
    private TextView mNewVersionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mButtonBackgroundView = findViewById(R.id.buttonbgView);
        mButtonBackgroundView.setTransitionName("buttonBackground");

        mUpdateContainer = (RelativeLayout) findViewById(R.id.updatecontainer);
        mNoUpdateContainer = (RelativeLayout) findViewById(R.id.noupdatecontainer);

        mCurrentVersionTextView = (TextView) findViewById(R.id.versioncurrent);

        mNewVersionTextView = (TextView) findViewById(R.id.version);
        mUpdateSubTitleTextView = (TextView) findViewById(R.id.updatesubtitle);
        Log.i("version", UpdaterUtil.getCurrentCMVersion());



    }


    @Override
    protected void onResume() {
        super.onResume();
        handleVersion();

    }

    public void launch(View view){
        Intent intent = new Intent(this, UpdateActivity.class);
        intent.putExtra("objectId", mNewVersionObject.getObjectId());
        ActivityOptions options = ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.getWidth(), view.getHeight());
        startActivity(intent, options.toBundle());
    }
    private void handleVersion(){



        ParseQuery<ParseObject> query = ParseQuery.getQuery("VersionObject");
        String currentVersion = UpdaterUtil.getCurrentCMVersion();

        query.whereGreaterThanOrEqualTo(ParseConstants.VERSION_NAME, currentVersion);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {
                    mCurrentVersionObject = parseObjects.get(0);

                    Log.d("updateCheck", "version found");
                    Log.d("updateInfo", mCurrentVersionObject.getDate("releaseDate").toString());

                    ParseQuery<ParseObject> newQuery = ParseQuery.getQuery("VersionObject");
                    newQuery.whereGreaterThan(ParseConstants.VERSION_RELEASE_DATE, mCurrentVersionObject.getDate(ParseConstants.VERSION_RELEASE_DATE));
                    newQuery.orderByDescending(ParseConstants.VERSION_RELEASE_DATE);
                    newQuery.setLimit(1);
                    newQuery.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> parseObjects, ParseException e) {
                            if(e== null){
                                if(parseObjects.size() == 0){
                                    Log.i("checkNew", "already the newest version");
                                    toggleContainerVisibility(false);
                                }
                                else{
                                    mNewVersionObject = parseObjects.get(0);
                                    Log.i("checkNew", "new available"+parseObjects.get(0).getDate(ParseConstants.VERSION_RELEASE_DATE));
                                    toggleContainerVisibility(true);

                                }
                            }
                        }
                    });
                }
            }
        });
    }

    private void toggleContainerVisibility(boolean available) {
        toggleTextVersion(available);

        if (available) {

            mUpdateContainer.setVisibility(View.VISIBLE);
            mNoUpdateContainer.setVisibility(View.GONE);
        } else {
            mUpdateContainer.setVisibility(View.GONE);
            mNoUpdateContainer.setVisibility(View.VISIBLE);
        }
    }

    private void toggleTextVersion(boolean available) {
        if (available) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            String date = df.format(mNewVersionObject.getDate(ParseConstants.VERSION_RELEASE_DATE));
            mUpdateSubTitleTextView.setText(date);
            mNewVersionTextView.setText("Version: "+mNewVersionObject.getString(ParseConstants.VERSION_NUMBER));

        } else {
            mCurrentVersionTextView.setText("Version: "+mCurrentVersionObject.getString(ParseConstants.VERSION_NUMBER));

        }
    }
}
